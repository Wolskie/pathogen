#!python
#----------------------------------
# Name:         Pathogen micro CGI framework
# Description:  Used to quickily develop (slow) CGI applications.
# Author:       Mark Hahl
# Version:      0.0.0
#

import cgi
import time
import sys
import os
import Templite
from urlparse import urlparse

class HTMLPage:

    @classmethod
    def body(self):
        return r"""
            <html>
                <head>
                    <title>Pathogen</title>
                <body>%s</body>
            </html>
        """

    @classmethod
    def error420(self):
        return self.body() % r"""
            <h2>Pathogen - Error</h2>
            <p>%s</p>
        """

    @classmethod
    def error404(self):
        return self.body() % r"""
            <h2>Resource not found</h2>
            <p>The resource you are looking for might have been 
            removed, had its name changed, or is temporarily unavailable.</p>
        """

class Response:

    def __init__(self, h="Content-Type: text/html\n",b = "<h2>There is a problem</h2>"):
        self.header = h
        self.body   = b

    def reply(self):
        print self.header
        print self.body

class Route:
    def __init__(self, method, path, block, args):
        self.method   = method.upper()
        self.path     = path
        self.block    = block
        self.args     = args
        self.response = None

    def call(self, params):
        self.response = self.block(self.args, params)
        return self.response

class RouteController:

    def __init__(self):
        self.routes  = []
        self.storage = cgi.FieldStorage()
        self.params  = self.convert_params(self.storage)

    def convert_params(self, storage):
        params = {}
        for key in storage.keys():
            params[ key ] = storage[ key ].value
        return params


    def add(self, method, route, block, args):
        self.routes.append(Route(method, route, block, args))

    def run(self):
        if not os.environ.has_key("REQUEST_URI"):
            return Response(
                "Content-Type: text/html\n",
                HTMLPage.error404()
            )

        for route in self.routes:
            u = urlparse(os.environ['REQUEST_URI'])
            if (u.path == route.path) and (os.environ['REQUEST_METHOD'] == route.method):
                return route.call(self.params)

        return Response(
            "Content-Type: text/html\n",
            HTMLPage.error404()
        )


class Pathogen:

    def __init__(self, base, url_base):
        self.incubate(base)

        self.base       = base
        self.controller = RouteController()
        self.response   = Response()
        self.url_base   = url_base

    def header(self,h):
        self.response.header = h

    def body(self, b):
        self.response.body = b

    def incubate(self, base):
        os.chdir(base)
        sys.path.append(base)

    def infect(self, method, url, callback, **kw):
        url = self.url_base + url
        self.controller.add(method, url, callback, kw['args'] )
    
    def spread(self):
        resp = self.controller.run()
        resp.reply()

    def render(self, path, **kw):

        template      = None
        templete_file = None

        try:
            template_file = open(path, "r").read()
            template = Templite.Templite(template_file)
        except Exception as err:
            self.body(
                HTMLPage.error420() % err
            )
            return

        try:
            self.body(template.render(kw))
        except NameError as e:
            self.body(e)
            return

